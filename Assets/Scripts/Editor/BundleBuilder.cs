﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BundleBuilder : Editor
{
   [MenuItem("Assets/Build Asset Bundle")]
   static void BuildAllAssetBundles() {
        BuildPipeline.BuildAssetBundles(@"F:\Desktop\AssetBundles", BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);
    }
}
