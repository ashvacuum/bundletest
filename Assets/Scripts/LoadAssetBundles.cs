﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAssetBundles : MonoBehaviour
{
    private AssetBundle _loadedAssetBundle;
    [SerializeField]private string bundleURL;
    [SerializeField] private string assetToUse;
    [SerializeField] private readonly string[] stringNames = {
        "stat_californiaroll","stat_ebinigiri","stat_maki",
        "stat_onigiri","stat_sushi","stat_tamagonigiri",
        "stat_tempura","stat_wasabi"};
    private Dictionary<string, Sprite> spriteDictionary;
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(AssetLoader());
    }

    IEnumerator AssetLoader() {
        yield return LoadAssetBundle(bundleURL);
        SetUpPhases();
    }

    // Update is called once per frame
    private bool LoadAssetBundle(string url)
    {
        _loadedAssetBundle = AssetBundle.LoadFromFile(url);
        return _loadedAssetBundle != null;
    }

    void SetUpPhases() {
        if (_loadedAssetBundle != null) {
            Sprite[] sprites = _loadedAssetBundle.LoadAssetWithSubAssets<Sprite>(assetToUse);
            spriteDictionary = new Dictionary<string, Sprite>();

            foreach(Sprite s in sprites) {
                spriteDictionary.Add(s.name, s);
            }
            int x = 0;
            foreach(string s in stringNames) {
                GameObject g = Instantiate(new GameObject(s), new Vector3(x, 0), Quaternion.identity);
                g.AddComponent<SpriteRenderer>().sprite = spriteDictionary[s];
                x++;
            }
        } else {
            Debug.Log("AssetBundle not loaded");
        }
    }
}
